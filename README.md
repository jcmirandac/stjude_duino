# stjude_duino

Arduino compatible board for education. An easy to use electronics prototyping board for school students. In development on St. Jude Fablab for St. Jude School.

The board will include:

  - LED grid output
   - DC motor output (with separate power)
   - I2C sensors input
   - PWM outputs
   - Digital and analog inputs/outputs

Eventually, a curriculum will be developed to go with the board.

![board preview](/STJDuino_V2.png)